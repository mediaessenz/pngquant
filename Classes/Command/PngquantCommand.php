<?php
namespace MEDIAESSENZ\Pngquant\Command;

use MEDIAESSENZ\Pngquant\Service\PngquantService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Pngquant Command.
 */
class PngquantCommand extends Command {

    /**
     * Configure the command by defining the name, options and arguments
     */
    public function configure()
    {
        $this
            ->setDescription('Process PNG Images')
            ->setHelp('Process PNG Images')
            ->addArgument('storageUid', InputArgument::OPTIONAL, 'storageUid',
                1)
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @return int
     * @throws InsufficientFolderAccessPermissionsException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pngquantService = GeneralUtility::makeInstance(PngquantService::class);
        $pngquantService->convertStorage((int)$input->getArgument('storageUid'));

        return 0;
    }
}
