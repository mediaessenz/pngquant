<?php

namespace MEDIAESSENZ\Pngquant\Slots;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Resource\Service\FileProcessingService;
use TYPO3\CMS\Core\Resource\Driver\DriverInterface;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use MEDIAESSENZ\Pngquant\Service\PngquantService;

/**
 * PNG post processing for processed images.
 */
class PngProcessing implements SingletonInterface {

	/**
	 * @var PngquantService
	 */
	protected $pngquantService;

	/**
	 *
	 */
	public function __construct() {
		$this->pngquantService = GeneralUtility::makeInstance(PngquantService::class);
	}

	/**
	 * Called from FileProcessingService signal dispatch, after image process.
	 *
	 * @param FileProcessingService $fileProcessingService
	 * @param DriverInterface $driver
	 * @param ProcessedFile $processedFile
	 * @param FileInterface $file
	 * @param string $context
	 * @param array $configuration
	 */
	public function postProcessFile(FileProcessingService $fileProcessingService, DriverInterface $driver, ProcessedFile $processedFile, FileInterface $file, string $context, array $configuration) {
		if($processedFile->exists() && !is_null($processedFile->getIdentifier()) && $processedFile->isUpdated()) {
			$this->pngquantService->convertPngImage($processedFile);
		}
	}
}
