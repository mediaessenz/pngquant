<?php

namespace MEDIAESSENZ\Pngquant\Hooks;

use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\File\ExtendedFileUtilityProcessDataHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\File\ExtendedFileUtility;
use MEDIAESSENZ\Pngquant\Service\PngquantService;

/**
 * File upload hook.
 */
class FileUploadHook implements ExtendedFileUtilityProcessDataHookInterface, SingletonInterface
{

    /**
     * @var PngquantService
     */
    protected $pngquantService;

    /**
     * @var array
     */
    protected $confArray = [];

    /**
     * FileUploadHook constructor.
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    public function __construct()
    {
        $this->pngquantService = GeneralUtility::makeInstance(PngquantService::class);
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        $this->confArray = $extensionConfiguration->get('pngquant');
    }

    /**
     * Hook for uploaded files.
     *
     * {@inheritDoc}
     * @see \TYPO3\CMS\Core\Utility\File\ExtendedFileUtilityProcessDataHookInterface::processData_postProcessAction()
     */
    public function processData_postProcessAction($action, array $cmdArr, array $result, ExtendedFileUtility $parentObject)
    {
        if (!$this->confArray['keepOriginal']) {
            $files = array_pop($result);
            if (is_array($files)) {
                foreach ($files as $file) {
                    /** @var $file FileInterface */
                    $this->pngquantService->convertPngImage($file);
                }
            }
        }
    }
}
