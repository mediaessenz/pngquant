<?php

namespace MEDIAESSENZ\Pngquant\Service;

use RuntimeException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\CommandUtility;
use TYPO3\CMS\Core\Resource\File;

/**
 * Pngquant service.
 */
class PngquantService implements SingletonInterface
{

    /**
     * @var string
     */
    const EXTENSION_PNG = 'png';

    /**
     * Pngquant command line template.
     * @var string
     */
    const COMMAND = '@EXECUTABLE@ @NOFS@ @OUTPUT@ @SPEED@ @QUALITY@ @IEBUG@ @INPUT@ --force';

    /**
     * @var Logger
     */
    protected $logger = null;

    /**
     * Extension configuration.
     * @var array
     */
    protected $confArray = [];

    /**
     * @var StorageRepository
     */
    protected $storageRepository = null;

    /**
     * PngquantService constructor.
     * Initializes logger and retrieve extension configuration.
     *
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    public function __construct()
    {
        $this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        $this->confArray = $extensionConfiguration->get('pngquant');
    }

    /**
     * Convert all PNG images (except processed) from specified storage.
     *
     * @param int $storageUid
     * @throws InsufficientFolderAccessPermissionsException
     */
    public function convertStorage(int $storageUid)
    {
        $this->storageRepository = GeneralUtility::makeInstance(StorageRepository::class);
        $storage = $this->storageRepository->findByUid($storageUid);
        if ($storage) {
            // Convert all storage files
            $storage->setEvaluatePermissions(false);
            $files = $storage->getFilesInFolder($storage->getRootLevelFolder(false), 0, 0, true, true);
            foreach ($files as $file) {
                if ($file instanceof File) {
                    $this->convertPngImage($file);
                }
                unset($file);
            }
        } else {
            $this->logger->error('No storage found', ['storage' => $storageUid]);
        }
    }

    /**
     * Convert PNG image using pngquant command.
     *
     * @param FileInterface $file
     */
    public function convertPngImage(FileInterface $file)
    {
        try {
            if (self::EXTENSION_PNG !== $file->getExtension()) {
                return;
            }

            // Ignore processed file which uses original file
            if ($file instanceof ProcessedFile && $file->usesOriginalFile()) {
                $this->logger->debug('Do not convert processed file identical with its original file', ['file' => $file]);
                return;
            }

            // Set input/output files for pngquant command
            // Input file is the the specified file we want to quantize
            // Output file is a temporary file in typo3temp directory
            $inputFilePath = $file->getForLocalProcessing();
            $outputFilePath = GeneralUtility::tempnam('sg_pngquant_', '.' . self::EXTENSION_PNG);

            // Build command line
            $cmd = $this->buildCommand($inputFilePath, $outputFilePath);

            $result = CommandUtility::exec($cmd, $output, $returnValue);
            if (0 === $returnValue) {
                // Replace content
                if ($file instanceof ProcessedFile) {
                    // For processed file, only convert real processed file (i.e. different than their original file)
                    // Temporary file is automatically removed when updating a processed file
                    $this->logger->debug('Update processed file', ['cmd' => $cmd]);
                    $file->updateWithLocalFile($outputFilePath);
                } elseif (!$this->confArray['keepOriginal']) {
                    // Convert original files according to extension configuration
                    // After conversion the temporary file is removed
                    $this->logger->debug('Update original file', ['cmd' => $cmd]);
                    $contents = @file_get_contents($outputFilePath);
                    $file->setContents($contents);
                }
            } else {
                $this->logger->error('Convert image', ['cmd' => $cmd, 'result' => $result, 'output' => $output, 'returnValue' => $returnValue]);
            }
            // Remove temporary file, if exists
            if (file_exists($outputFilePath)) {
                $this->removeTemporaryFile($outputFilePath);
            }
        } catch (RuntimeException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Remove temporary file.
     *
     * @param string $filePath
     */
    protected function removeTemporaryFile(string $filePath)
    {
        if (!GeneralUtility::unlink_tempfile($filePath)) {
            $this->logger->error('Failed to remove file', ['filepath' => $filePath]);
        }
    }

    /**
     * Build pngquant command.
     *
     * @param string $inputFilePath
     * @param string $outputFilePath
     * @return string
     */
    protected function buildCommand(string $inputFilePath, string $outputFilePath): string
    {
        // Build parameters using configuration
        $executable = $this->confArray['executable'];
        $nofs = $this->confArray['nofs'] ? '--nofs' : '';
        $speed = '--speed ' . $this->confArray['speed'];
        $quality = $this->confArray['quality'] ? '--quality ' . $this->confArray['quality'] : '';
        $iebug = $this->confArray['iebug'] ? '--iebug' : '';
        $output = '--output=' . $outputFilePath;

        // Build command
        return str_replace(
            ['@EXECUTABLE@', '@NOFS@', '@OUTPUT@', '@SPEED@', '@QUALITY@', '@IEBUG@', '@INPUT@'],
            [$executable, $nofs, $output, $speed, $quality, $iebug, $inputFilePath],
            self::COMMAND);
    }
}
