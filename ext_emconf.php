<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "pngquant"
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['pngquant'] = [
    'title' => 'pngquant',
    'description' => 'Compress png images using pngquant library (lossy compression of PNG images - https://pngquant.org/).',
    'category' => 'backend',
    'version' => '2.0.0',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.14-10.4.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
